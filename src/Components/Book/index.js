import { useContext } from "react";
import { CartContext } from "../../context";

const Book = ({ items, cartItem }) => {
  let { myCart, setMyCart } = useContext(CartContext);

  const addToCart = (el) => {
    let arr = [el];
    let newArr = arr.concat(myCart);
    setMyCart(newArr);

    let memCart = JSON.parse(localStorage.getItem("cart"));
    memCart.push(el);
    localStorage.setItem("cart", JSON.stringify(memCart));
  };

  return (
    <div className="book">
      <img src={items.cover_url} alt="pcover_book" />
      <hr />
      <p>Tytuł: {items.title}</p>
      <hr />
      <p>Autor: {items.author}</p>
      <hr />
      <p>Liczba stron: {items.pages}</p>
      <hr />

      <button
        onClick={() => {
          addToCart(items);
        }}
      >
        Dodaj do koszyka
      </button>
    </div>
  );
};

export default Book;
