import { useContext, useState } from "react";
import { CartContext } from "../../context";

const Summary = ({ setShowSummary }) => {
  let [name, setName] = useState("");
  let [surname, setSurname] = useState("");
  let [city, setCity] = useState("");
  let [postCode, setPostCode] = useState("");

  let myOrder = {
    order: [],
    first_name: name,
    last_name: surname,
    city: city,
    zip_code: postCode,
  };
  const { myCart,setMyCart } = useContext(CartContext);
  let price = 0;

  myCart.map((el) => {
    price = price + el.price;
    myOrder.order.push({ id: el.id, quantity: 1 });
    console.log(myOrder);
    
  });
  const handleSubmit = (e) => {
    e.preventDefault();
    if (
      /^[A-Za-zżźćńółęąśŻŹĆĄŚĘŁÓŃ]{4,25}$/.test(name) &
      /^[A-Za-zżźćńółęąśŻŹĆĄŚĘŁÓŃ]{5,25}$/.test(surname) &
      /^[A-Za-zżźćńółęąśŻŹĆĄŚĘŁÓŃ]+$/.test(city) &
      /^[0-9]{2}-[0-9]{3}$/.test(postCode)
    ) {
      fetch("http://localhost:3001/api/order", {
        method: "post",
        headers: {
          "Access-Control-Request-Headers": "*",
          "Access-Control-Request-Method": "*",
          "Content-Type": "application/json",
        },

        body: JSON.stringify(myOrder),
      })
        .then((response) => response.json())
        .then((res) => {
if(res.data){alert('Zamówienie zostało złożone');
setShowSummary(false);
setMyCart([]);
localStorage.setItem('cart',[])
}
else{alert('Wystąpił błąd, sprobuj jeszcze raz')}


        })
        
        .catch((err) => console.log(err)); //obsługa błędu

      console.log(myOrder);
    } else {
      alert("NIEPOPRAWNE DANE, POPRAW DANE I SPROBÓJ PONOWNIE");
    }
  };

  return (
    <div className="summary">
      <h1>Podsumowanie</h1>
      <p>
        Twoje zamówienie na kwotę <strong>{price}</strong> zł
      </p>

      <form onSubmit={(e) => handleSubmit(e)}>
        <input
          className={`summary_input ${
            !/^[A-Za-zżźćńółęąśŻŹĆĄŚĘŁÓŃ]{4,25}$/.test(name) ? "error" : ""
          }`}
          type="text"
          value={name}
          onChange={(e) => setName(e.target.value)}
          placeholder="Imię, np: Tomasz"
        />
        <br />
        <input
          className={`summary_input ${
            !/^[A-Za-zżźćńółęąśŻŹĆĄŚĘŁÓŃ]{5,25}$/.test(surname) ? "error" : ""
          }`}
          type="text"
          value={surname}
          onChange={(e) => setSurname(e.target.value)}
          placeholder="Nazwisko, np: Kowalski"
        />
        <br />
        <input
          className={`summary_input ${
            !/^[A-Za-zżźćńółęąśŻŹĆĄŚĘŁÓŃ]+$/.test(city) ? "error" : ""
          }`}
          type="text"
          value={city}
          onChange={(e) => setCity(e.target.value)}
          placeholder="Miasto, np: Bytom"
        />
        <br />
        <input
          className={`summary_input ${
            !/^[0-9]{2}-[0-9]{3}$/.test(postCode) ? "error" : ""
          }`}
          type="text"
          value={postCode}
          onChange={(e) => setPostCode(e.target.value)}
          placeholder="Kod pocztowy w formacie 00-000"
        />
        <br />
        <input className={`summary_submit`} type="submit" value="Wyślij" />
      </form>

      <button
        onClick={() => {
          setShowSummary(false);
        }}
      >
        Zamknij
      </button>
    </div>
  );
};

export default Summary;
