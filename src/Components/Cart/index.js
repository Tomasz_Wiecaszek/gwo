import React, { useContext} from "react";
import { CartContext } from "../../context";

const Cart = ({ setShowCart, setShowSummary }) => {
  let { myCart, setMyCart } = useContext(CartContext);


  const deleteItem = (index) => {
    let arr = myCart.slice();
    arr.splice(index, 1);
    setMyCart(arr);

    let memCart = JSON.parse(localStorage.getItem("cart"));
    memCart.splice(index, 1);
    localStorage.setItem("cart", JSON.stringify(memCart));
  };

  return (
    <div className="cart">
      {myCart.length > 0 ? (
        myCart.map((el, index) => {
          return (
            <div className="cart_item" key={Math.random()}>
              <img src={el.cover_url} alt="cover" />
              <p>Tytuł: {el.title} </p>
              <p>Cena: {el.price} zł</p>
              <button
                onClick={() => {
                  deleteItem(index);
                }}
              >
                Usuń pozycję
              </button>
            </div>
          );
        })
      ) : (
        <h1>Koszyk jest pusty</h1>
      )}

      {myCart.length > 0 ? (
        <button
          className="cart_button_next"
          onClick={() => {
            setShowSummary(true);
            setShowCart(false);
          }}
        >
          Dalej
        </button>
      ) : (
        <></>
      )}
      <button
        className="cart_button_close"
        onClick={() => {
          setShowCart(false);
        }}
      >
        Zamknij
      </button>
    </div>
  );
};

export default Cart;
