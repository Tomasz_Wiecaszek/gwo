import React, { createContext, useState, useEffect } from "react";

export const Items = createContext({
  item: [],
  setItem: (item) => {},
});

export const ItemProvider = ({ children }) => {
  useEffect(() => {
    fetch("http://localhost:3001/api/book")
      .then((res) => res.json())
      .then((result) => {
        setItem(result.data);
      })
      .catch((err) => console.log(err)); //obsługa błędu
  }, []);

  const [item, setItem] = useState([]);

  return (
    <Items.Provider
      value={{
        item,
      }}
    >
      {children}
    </Items.Provider>
  );
};

export const CartContext = createContext({
  myCart: [],
  setMyCart: (item) => {},
});

export const CartProvider = ({ children }) => {
  const [myCart, setMyCart] = useState(
    localStorage.getItem("cart") ? JSON.parse(localStorage.getItem("cart")) : []
  );

  return (
    <CartContext.Provider
      value={{
        myCart,
        setMyCart,
      }}
    >
      {children}
    </CartContext.Provider>
  );
};
