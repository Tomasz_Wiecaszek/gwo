import { ItemProvider, CartProvider } from "./context";
import "./styles/themes/default/theme.scss";

import Homepage from "./pages/Homepage";
function App() {
  return (
    <div>
      <CartProvider>
        <ItemProvider>
          <Homepage />
        </ItemProvider>
      </CartProvider>
    </div>
  );
}

export default App;
