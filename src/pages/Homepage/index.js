import React, { useState, useEffect, useContext } from "react";
import Book from "../../Components/Book";
import Cart from "../../Components/Cart";
import Summary from "../../Components/Summary";
import { Items } from "../../context";
import { CartContext } from "../../context";
const Homepage = () => {
  let { myCart } = useContext(CartContext);
  const [showCart, setShowCart] = useState(false);
  const [showSummary, setShowSummary] = useState(false);
  //tworzenie tablicy w local storage
  useEffect(() => {
    if (!localStorage.getItem("cart")) {
      localStorage.setItem("cart", JSON.stringify([]));
    }
  }, []);

  const { item } = useContext(Items);

  return (
    <div className="main__wrapper">
      <header className="header">
        <h1>NASZ ASORTYMENT</h1>
        <button
          onClick={() => {
            myCart.length > 0
              ? setShowCart(!showCart)
              : alert("Koszyk jest pusty");
          }}
        >
          Koszyk(<b>{myCart.length}</b>)
        </button>
      </header>
      <div className="main__wrapper--books">
        {item.map((bookArray) => {
          return (
            <React.Fragment key={Math.random()}>
              <Book items={bookArray} />
            </React.Fragment>
          );
        })}
      </div>
      {showSummary ? <Summary setShowSummary={setShowSummary} /> : <></>}
      {showCart ? (
        <Cart setShowCart={setShowCart} setShowSummary={setShowSummary} />
      ) : (
        <></>
      )}
    </div>
  );
};

export default Homepage;
